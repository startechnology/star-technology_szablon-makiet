var KORA_skrypt = document.currentScript.src;
var KORA_sciezka = KORA_skrypt.substring(0, KORA_skrypt.lastIndexOf('/js/')) + '/';

var KORA_biblioteki = {
	'tooltipster': false,
	'selectize': false,
	'jquery-ui': false,
}

function KORA_init() {
	$('head').append($('<link rel="stylesheet" type="text/css" />').attr('href', KORA_sciezka + 'css/KORA.css'));
	
	/**
	 * tooltipster
	 */
	$('head').append($('<link rel="stylesheet" type="text/css" />').attr('href', KORA_sciezka + 'libs/tooltipster/css/tooltipster.bundle.min.css'));
	$('head').append($('<link rel="stylesheet" type="text/css" />').attr('href', KORA_sciezka + 'css/tooltipster_KORA.css'));
	$.getScript(KORA_sciezka + 'libs/tooltipster/js/tooltipster.bundle.min.js', function () {
		KORA_biblioteki['tooltipster'] = true;
	});
	
	/**
	 * selectize
	 */
	$('head').append($('<link rel="stylesheet" type="text/css" />').attr('href', KORA_sciezka + 'libs/selectize/css/selectize.css'));
	$('head').append($('<link rel="stylesheet" type="text/css" />').attr('href', KORA_sciezka + 'css/selectize_KORA.css'));
	$.getScript(KORA_sciezka + 'libs/selectize/js/selectize.min.js', function () {
		KORA_biblioteki['selectize'] = true;
	});
	
	/**
	 * jquery-ui
	 */
	$('head').append($('<link rel="stylesheet" type="text/css" />').attr('href', KORA_sciezka + 'libs/jquery-ui/css/jquery-ui.min.css'));
	$('head').append($('<link rel="stylesheet" type="text/css" />').attr('href', KORA_sciezka + 'css/jquery-ui_KORA.css'));
	$.getScript(KORA_sciezka + 'libs/jquery-ui/js/jquery-ui.min.js', function () {
		KORA_biblioteki['jquery-ui'] = true;
		jQuery(function ($) {
			$.datepicker.regional['pl'] = {
				closeText: 'Zamknij',
				prevText: '&#x3C;Poprzedni',
				nextText: 'Następny&#x3E;',
				currentText: 'Dziś',
				monthNames: ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'],
				monthNamesShort: ['Sty', 'Lu', 'Mar', 'Kw', 'Maj', 'Cze', 'Lip', 'Sie', 'Wrz', 'Pa', 'Lis', 'Gru'],
				dayNames: ['Niedziela', 'Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek', 'Sobota'],
				dayNamesShort: ['Nie', 'Pn', 'Wt', 'Śr', 'Czw', 'Pt', 'So'],
				dayNamesMin: ['N', 'Pn', 'Wt', 'Śr', 'Cz', 'Pt', 'So'],
				weekHeader: 'Tydz',
				dateFormat: 'yy-mm-dd',
				firstDay: 1,
				isRTL: false,
				showMonthAfterYear: false,
				yearSuffix: ''};
			$.datepicker.setDefaults($.datepicker.regional['pl']);
		});
	});

}

function KORA_narzedzia() {
	$('body').append('<div id="KORA_Narzedzia"></div>');
	$('div#KORA_Narzedzia').append('<label><input type="checkbox" id="KORA_Narzedzia_Zmiany"> Oznaczenia zmian</label>');
	
	var zmiany = $('input#KORA_Narzedzia_Zmiany');
	zmiany.on('change', function() {
		if (zmiany.prop('checked')) {
			$('[data-label~="@zmiana"]').show();
		} else {
			$('[data-label~="@zmiana"]').hide();
		}
		localStorage.setItem('KORA_Narzedzia_Zmiany', JSON.stringify(zmiany.prop('checked')));
	});
	zmiany.prop('checked', JSON.parse(localStorage.getItem('KORA_Narzedzia_Zmiany'))).change();
	
}

function KORA_strona() {
	/**
	 * @menuRozwijane
	 */
	$('[data-label~="@menuRozwijane"]').each(function () {
		$(this).find('div.table_cell').each(function () {
			$(this).css('background-color', 'black').css('cursor', 'pointer');
			if (!$(this).is(':first-child')) {
				$(this).css('border-top', '1px solid #CCC');
			}
			$(this).mouseover(function () {
				$(this).find('img').css('opacity', '0.9');
			});
			$(this).mouseout(function () {
				$(this).find('img').css('opacity', '1');
			});
		});
	});
	
	/**
	 * @tooltip:"TEKST_PODPOWIEDZI"
	 */
	$('[data-label*="@tooltip"]').each(function () {
		var pattern = /tooltip:"([^"]+)"/;
		var text = pattern.exec($(this).data('label'))[1];
		$(this).tooltipster({
			animationDuration: 0,
			delay: 0,
			content: text,
			distance: 0,
			theme: 'tooltipster-KORA',
			contentAsHTML: true,
		});
	});
	
	/**
	 * @przycisk
	 */
	$('[data-label~="@przycisk"]').each(function () {
		$(this).css('cursor', 'pointer');
		var background;
		$(this).mouseenter(function () {
			background = $(this).find('div').css('background');
			$(this).find('div').css('background', '#cff0ff');
		}).mouseleave(function () {
			$(this).find('div').css('background', background);
		});
	});
	
	/**
	 * @przyciskPomocniczy
	 */
	$('[data-label~="@przyciskPomocniczy"]').each(function () {
		$(this).css('cursor', 'pointer');
		var background;
		$(this).mouseenter(function () {
			background = $(this).find('div').css('background');
			$(this).find('div').css('background', '#ddd');
		}).mouseleave(function () {
			$(this).find('div').css('background', background);
		});
	});
	
	/**
	 * @link
	 */
	$('[data-label~="@link"]').each(function () {
		$(this).css('cursor', 'pointer');
	});
	
	/**
	 * @zmiana
	 */
	$('[data-label~="@zmiana"]').each(function () {
		$(this).css('opacity', '1').css('color', '#666').css('pointer-events', 'none');
		$(this).find('div').each(function () {
			if ($(this).is(':first-child')) {
				$(this).css('background', 'rgba(255, 255, 0, 0.25)');
			}
		});
	});
	
	/**
	 * @naglowekSekcji
	 */
	$('[data-label~="@naglowekSekcji"]').each(function () {
		$(this).find('img').remove();
		$(this).find('[data-label~="@naglowekSekcji-tytul"]').find('p').css('margin-top', '7px');
		$(this).find('[data-label~="@naglowekSekcji-linia"]').css('border-bottom', '2px solid #CCC');
	});
}

function KORA_tabelaLista() {
	/**
	 * @tabelaLista
	 */
	var tabela = $('[data-label~="@tabelaLista"]');
	tabela.find('img').remove();
	tabela.find('div.table_cell').css('border-bottom', '1px solid #999');
	
	/**
	 * @tabelaLista-lp
	 */
	var lp = 0;
	tabela.find('div[data-label~="@tabelaLista-lp"]').each(function () {
		if (lp > 0) {
			$(this).find('span').html(lp + '.');
		}
		lp++;
	});
	tabela.find('div[data-label~="@tabelaLista-lp"][data-label~="@tabelaLista-podsumowanie"]').each(function () {
			$(this).find('span').html('');
	});
	
	/**
	 * @tabelaLista-trybik
	 */
	tabela.find('div[data-label~="@tabelaLista-trybik"]').each(function () {
		$(this).find('span').html('');
		$(this).css('background-image', 'url(' + KORA_sciezka + 'img/trybik.png)');
		$(this).css('background-size', '14px 14px');
		$(this).css('background-repeat', 'no-repeat');
		$(this).css('background-position', 'center');
	});
	tabela.find('div[data-label~="@tabelaLista-trybik"][data-label~="@tabelaLista-naglowek"]').each(function () {
		$(this).css('background', 'none');
	});
	tabela.find('div[data-label~="@tabelaLista-trybik"][data-label~="@tabelaLista-podsumowanie"]').each(function () {
		$(this).css('background', 'none');
	});
	
	/**
	 * @tabelaLista-checkbox
	 */
	tabela.find('div[data-label~="@tabelaLista-checkbox"]').each(function () {
		$(this).find('span').html('<input type="checkbox">');
	});
	tabela.find('div[data-label~="@tabelaLista-checkbox"][data-label~="@tabelaLista-naglowek"]').each(function () {
		$(this).find('input').each(function () {
			$(this).click(function () {
				if ($(this).is(':checked')) {
					tabela.find('input[type="checkbox"').prop('checked', true);
				} else {
					tabela.find('input[type="checkbox"').prop('checked', false);
				}
			});
		});
	});
	tabela.find('div[data-label~="@tabelaLista-checkbox"][data-label~="@tabelaLista-podsumowanie"]').each(function () {
		$(this).find('span').html('');
	});
	
	/**
	 * @tabelaLista-naglowek
	 */
	tabela.find('div[data-label~="@tabelaLista-naglowek"]').each(function () {
		$(this).css('border-bottom', '2px solid black');
	});
	
	/**
	 * @tabelaLista-stopka
	 */
	tabela.find('div[data-label~="@tabelaLista-stopka"]').each(function () {
		$(this).css('border-bottom', '2px solid black');
	});
	
	/**
	 * @tabelaLista-podsumowanie
	 */
	tabela.find('div[data-label~="@tabelaLista-podsumowanie"]').each(function () {
		$(this).css('border-top', '2px solid black');
		$(this).css('border-bottom', '2px solid black');
		$(this).css('background-color', '#cff0ff');
	});
}

function KORA_tabelaPodglad() {
	/**
	 * @tabelaPodglad
	 */
	var tabela = $('[data-label~="@tabelaPodglad"]');
	tabela.find('img').remove();
}

function KORA_formularz() {
	$('div[data-label~="@pole-data"]').each(function () {
		$(this).find('input').datepicker({
			onSelect: function () {
				$(this).keyup();
				$(this).blur();
			}
		}).keyup(function () {
			$(this).css('color', 'black');
		});
	});
	$('div[data-label~="@pole-autoWybor"]').each(function () {
		$(this).find('select').selectize();
	});
	
}

function KORA_elementy() {
	if (
		KORA_biblioteki['tooltipster'] 
		&& KORA_biblioteki['selectize'] 
		&& KORA_biblioteki['jquery-ui']
	) {
		KORA_strona();
		KORA_tabelaLista();
		KORA_tabelaPodglad();
		KORA_formularz();
		KORA_narzedzia();
		clearInterval(KORA_wczytywanie);
	}
}

KORA_init();
var KORA_wczytywanie = setInterval(KORA_elementy, 100);
