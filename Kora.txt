* Informacje o szablonie:

	- Szerokość strony: 980px
	- Wyrównanie strony: do środka
	
----

Lista ID i klas elementów z biblioteki KORA:

#ID
	- powinien być tylko jeden element z ID na stronie!

@klasa

---



Ogólne klasy stosowane do wielu elementów:

@menuRozwijane
@przycisk
@przyciskPomocniczy
@link
@naglowek
@tooltip:TEKST_PODPOWIEDZI
@zmiana

---

#STRONA_DOMOWA

@nawigacja-pozycja
	@nawigacja-pozycja-tytul
	@nawigacja-pozycja-menu
		@nawigacja-pozycja-menu-tytul
		@nawigacja-pozycja-menu-link
		
#MENU_UZYTKOWNIKA
	@menuUzytkownika-tytul
	@menuUzytkownika-menu
	
@paginacja
	@paginacja-iloscNaStronie
	@paginacja-numerStrony
	
#STOPKA

#POPUP
	@popup-tytul
	@popup-zamknij
	@popup-tekst
	@popup-ok
	@popup-anuluj
	
	#POPUP_POTWIERDZENIE
		#POPUP_POTWIERDZENIE_TYTUL
		#POPUP_POTWIERDZENIE_TEKST
		#POPUP_POTWIERDZENIE_OK
		#POPUP_POTWIERDZENIE_ANULUJ
		
@menuWieluAkcji-przycisk
@menuWieluAkcji-menu
@menuWieluAkcji-menu-link

@multiakcja
	@multiakcja-select
	@multiakcja-ok

@tabelaLista
	@tabelaLista-naglowek
		@tabelaLista-naglowek-sortowanie:TEKST_SORTOWANIA
	@tabelaLista-stopka
	@tabelaLista-podsumowanie
	
	@tabelaLista-checkbox
	@tabelaLista-lp
	@tabelaLista-trybik
	@tabelaLista-trybik-menu
	
	@tabelaLista-komorka
	
@tabelaPodglad
	@tabelaPodglad-nazwa
	@tabelaPodglad-wartosc
	
@naglowekSekcji
	@naglowekSekcji-tytul
	@naglowekSekcji-linia
	
	
@pole-nazwa
@pole-glowne
@pole-tekst
@pole-tekstWiele
@pole-data
@pole-wybor
@pole-autoWybor
@pole-multiWybor
@pole-radio
@pole-checkbox