# Szablon makiet Kora

## I. Działanie szablonu makiet

1. Jak zainstalować szablon makiet KORA
	* Skopiowanie folderu KORA
	* Uruchamianie skryptów poprzez OnPageLoad w elemencie master "Nagłówek"
	* Opis jak działają @tagi
2. Struktura plików w folderze KORA
3. Czym różni się obecny szablon od poprzednich wersji - nie trzeba edytować plików, tylko adnotacje w OnPageLoad i skopiowanie folderu "KORA"
4. Generowanie plików HTML i skopiowanie folderu "Kora"

## II. Bibloteka Kora

1. Ładowanie bibloteki
2. Opis poszczególnych elementów w biblotece
3. Dlaczego niektóre elementy są większe niż ich zawartość - opis automatycznych marginesów

## III. Elementy "Masters"

1. Opis poszczególnych elementów master
	* Dlaczego nagłowek jest bez nawigacji - bo widok logowania
	* Dodawanie stanu do okna popup - dodawanie formularza
2. Opis dlaczego elementy np. "Paginacja" lub "Opcje widoku" są elementami master jeżeli są już w biblotece

## IV. Właściwa makieta

1. Logowanie
	* Oprogramowanie przycisku "Zaloguj"
2. CRUDL / Lista
	* @tagi w widoku w poszczególnych elmentach - automatyczne style np. w tabeli
	* Automatyczne tworzenie Checkboxów, Lp. i trybika
	* Oprogramowanie trybika w tabeli
	* Oprogramowanie multiakcji - warunki w select i przycisk "Ok"
	* Opcje widoku - ustawienie stanu DRUKUJ / EKSPORTUJ w OnPageLoad na stronie
3. CRUDL / Podgląd
	* @tagi w widoku w poszczególnych elmentach - automatyczne style np. w tabeli
	* Breadcrumbs
	* Oprogramowanie multiakcji
	* Dlaczego "Powrót" jest takie duże - opis automatycznych marginesów
4. CRUDL / Dodaj
	* @tagi w widoku w poszczególnych elementach - automatyczne style i widgety typu datapicker
	* Dostępne pola w formularzu
	* Jakich elementów brakuje - pytanie do widzów xDDDD
	* Dlaczego "Dodaj lub anuluj" jest takie duże - opis automatycznych marginesów
5. CRUDL / Edytuj
	* Praktycznie to samo co w CRUDL / Dodaj, tylko wypełnione

## V. Czego brakuje

1. Customowe elementy?